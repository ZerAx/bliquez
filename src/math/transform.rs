use cgmath::{Matrix4, Quaternion, Vector3, Zero};

pub struct Transform {
    pub position: Vector3<f32>,
    pub scale: Vector3<f32>,
    pub rotation: Quaternion<f32>,
}

impl Default for Transform {
    fn default() -> Self {
        Self {
            position: Vector3::zero(),
            scale: Vector3::zero(),
            rotation: Quaternion::from_sv(1.0, Vector3::zero()),
        }
    }
}

impl Transform {
    pub fn combination(&self) -> Matrix4<f32> {
        Matrix4::from_translation(self.position)
            * Matrix4::from(self.rotation)
            * Matrix4::from_nonuniform_scale(self.scale.x, self.scale.y, self.scale.z)
    }
}
