use super::Rect2D;

pub struct Anchor {
    pub x_min: f32,
    pub x_max: f32,
    pub y_min: f32,
    pub y_max: f32,
}

pub struct Measure {
    pub anchor: Anchor,
    pub padding: Rect2D<f32>,
}

impl Default for Measure {
    fn default() -> Self {
        Self {
            anchor: Anchor {
                x_min: 0.0,
                x_max: 1.0,
                y_min: 0.0,
                y_max: 1.0,
            },
            padding: Rect2D {
                left: 0.0,
                right: 0.0,
                bottom: 0.0,
                top: 0.0,
            },
        }
    }
}

impl Measure {
    pub fn measure_rect(&self, parent: &Rect2D<f32>) -> Rect2D<f32> {
        let left = parent.left + parent.width() * self.anchor.x_min + self.padding.left;
        let right = parent.left + parent.width() * self.anchor.x_max - self.padding.right;
        let bottom = parent.bottom + parent.height() * self.anchor.y_min + self.padding.bottom;
        let top = parent.bottom + parent.height() * self.anchor.y_max - self.padding.top;

        Rect2D {
            left,
            right,
            bottom,
            top,
        }
    }
}

pub struct MeasuredRect {
    rect: Rect2D<f32>,
    measure: Measure,
}

impl MeasuredRect {
    pub fn recalculate(&mut self, parent: &Rect2D<f32>) {
        self.rect = self.measure.measure_rect(parent);
    }

    pub fn rect(&self) -> Rect2D<f32> {
        self.rect
    }

    pub fn measure(&self) -> &Measure {
        &self.measure
    }

    pub fn measure_mut(&mut self) -> &mut Measure {
        &mut self.measure
    }
}
