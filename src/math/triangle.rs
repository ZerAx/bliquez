use cgmath::Vector3;

#[derive(Clone, Copy)]
pub struct Triangle {
    pub points: [Vector3<f32>; 3],
}
