use cgmath::Vector3;

#[derive(Clone, Copy)]
pub struct Plane {
    pub origin: Vector3<f32>,
    pub normal: Vector3<f32>,
}
