pub const SPIRTE_SRC: &str = include_str!("../../builtin_shaders/sprite.wgsl");
pub const COLOR_2D_SRC: &str = include_str!("../../builtin_shaders/color2d.wgsl");
pub const COLOR_3D_SRC: &str = include_str!("../../builtin_shaders/color3d.wgsl");