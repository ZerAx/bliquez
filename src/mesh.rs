use std::ops::{Deref, DerefMut};

use mark_dirty::DirtyMark;

use crate::Vertex;

use super::buffer::DynBuffer;

pub struct Mesh<V: Vertex> {
    vertices: Vec<V>,
    indices: Vec<u32>,
}

impl<V: Vertex> Default for Mesh<V> {
    fn default() -> Self {
        Self::new()
    }
}

impl<V: Vertex> Mesh<V> {
    pub fn new() -> Self {
        Self {
            vertices: vec![],
            indices: vec![],
        }
    }

    pub fn vertices(&self) -> &[V] {
        &self.vertices
    }

    pub fn indices(&self) -> &[u32] {
        &self.indices
    }

    pub fn vertices_mut(&mut self) -> &mut Vec<V> {
        &mut self.vertices
    }

    pub fn indices_mut(&mut self) -> &mut Vec<u32> {
        &mut self.indices
    }

    pub fn vertex_count(&self) -> u32 {
        self.vertices().len() as u32
    }

    pub fn index_count(&self) -> u32 {
        self.indices().len() as u32
    }

    pub fn clear(&mut self) {
        self.vertices_mut().clear();
        self.indices_mut().clear();
    }

    pub fn add<IV, II>(&mut self, vertices: IV, indices: II)
    where
        IV: IntoIterator<Item = V>,
        II: IntoIterator<Item = u32>,
    {
        let count = self.vertex_count();
        self.vertices_mut().extend(vertices);
        self.indices_mut()
            .extend(indices.into_iter().map(|i| i + count));
    }

    pub fn add_mesh(&mut self, mesh: &Mesh<V>) {
        self.add(mesh.vertices.iter().copied(), mesh.indices.iter().copied())
    }

    pub fn add_vertex(&mut self, vertex: V) {
        let count = self.vertex_count();
        self.vertices_mut().push(vertex);
        self.indices_mut().push(count);
    }

    pub fn add_triangle(&mut self, triangle: [V; 3]) {
        let count = self.vertex_count();
        self.vertices_mut().extend(triangle);
        self.indices_mut().extend([count, count + 1, count + 2]);
    }

    /// add quad with lb, lt, rt, rb order
    pub fn add_quad(&mut self, quad: [V; 4]) {
        let count = self.vertex_count();
        self.vertices_mut().extend(quad);
        self.indices_mut()
            .extend([count, count + 1, count + 2, count, count + 2, count + 3]);
    }

    pub fn add_quad_frame(&mut self, quad: [V; 4]) {
        let count = self.vertex_count();
        self.vertices_mut().extend(quad);
        self.indices_mut().extend([
            count,
            count + 1,
            count + 1,
            count + 2,
            count + 2,
            count + 3,
            count + 3,
            count,
        ]);
    }

    /// order: 0flb 1flt 2frt 3frb 4brb 5blb 6blt 7brt
    pub fn add_cube_frame(&mut self, cube: [V; 8]) {
        let count = self.vertex_count();
        self.vertices_mut().extend(cube);
        self.indices_mut().extend([
            count,
            count + 1,
            count + 1,
            count + 2,
            count + 2,
            count + 3,
            count + 3,
            count,
            count + 4,
            count + 5,
            count + 5,
            count + 6,
            count + 6,
            count + 7,
            count,
            count + 5,
            count + 1,
            count + 6,
            count + 2,
            count + 7,
            count + 3,
            count + 4,
        ]);
    }
}

pub struct MeshInstance<V: Vertex> {
    mesh: DirtyMark<Mesh<V>>,
    vertex_buffer: DynBuffer<V>,
    index_buffer: DynBuffer<u32>,
}

impl<V: Vertex> DerefMut for MeshInstance<V> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.mesh
    }
}

impl<V: Vertex> Deref for MeshInstance<V> {
    type Target = Mesh<V>;

    fn deref(&self) -> &Self::Target {
        &self.mesh
    }
}

impl<V: Vertex> MeshInstance<V> {
    pub fn new(ctx: &super::Context) -> Self {
        Self::with_capacity(ctx, 0, 0)
    }

    pub fn with_capacity(ctx: &super::Context, vertex_count: usize, index_count: usize) -> Self {
        let vertex_buffer = DynBuffer::new(ctx, vertex_count, wgpu::BufferUsages::VERTEX);
        let index_buffer = DynBuffer::new(ctx, index_count, wgpu::BufferUsages::INDEX);

        Self {
            mesh: DirtyMark::new(Mesh::new()),
            vertex_buffer,
            index_buffer,
        }
    }

    pub fn prepare(&mut self, ctx: &super::Context) {
        if self.mesh.is_dirty() {
            self.vertex_buffer.write(ctx, self.mesh.vertices());
            self.index_buffer.write(ctx, self.mesh.indices());

            self.mesh.unmark_dirty();
        }
    }

    pub fn render<'p>(&'p self, pass: &mut wgpu::RenderPass<'p>) {
        pass.set_vertex_buffer(0, self.vertex_buffer.slice(..));
        pass.set_index_buffer(self.index_buffer.slice(..), wgpu::IndexFormat::Uint32);
        pass.draw_indexed(0..self.mesh.index_count(), 0, 0..1);
    }
}
