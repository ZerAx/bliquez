struct VertexInput {
    @location(0) position: vec2<f32>,
    @location(1) tex_coord: vec2<f32>,
}

struct Varying {
    @builtin(position) clip_pos: vec4<f32>,
    @location(0) tex_coord: vec2<f32>,
}

@group(0) @binding(0)
var<uniform> transform: mat4x4<f32>;

@vertex
fn vert(in: VertexInput) -> Varying {
    var out: Varying;
    out.clip_pos = transform * vec4(in.position, 0.0, 1.0);
    out.tex_coord = in.tex_coord;
    return out;
}


@group(1) @binding(0)
var font_cache: texture_2d<u32>;
@group(1) @binding(1)
var<uniform> toggle: u32;

@fragment
fn frag(in: Varying) -> @location(0) vec4<f32> {
    if toggle == 1 { return vec4(1.0); }

    let pix = textureLoad(font_cache, vec2(u32(in.tex_coord.x), u32(in.tex_coord.y)), 0);
    if pix.r == 0 {
        discard;
    }
    return vec4(1.0);
}