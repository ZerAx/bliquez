struct VertexInput {
    @location(0) position: vec2<f32>,
    @location(1) color: vec4<f32>,
};

@group(0) @binding(0)
var<uniform> model: mat4x4<f32>;

struct Varying {
    @builtin(position) clip_pos: vec4<f32>,
    @location(0) color: vec4<f32>,
};

@vertex
fn vert(in: VertexInput) -> Varying {
    var out: Varying;
    out.clip_pos = model * vec4<f32>(in.position, 0.0, 1.0);
    out.color = in.color;
    return out;
}

@fragment
fn frag(in: Varying) -> @location(0) vec4<f32> {
    return in.color;
}