struct VertexData {
    @location(0) position: vec2<f32>,
    @location(1) color: vec4<f32>,
    @location(2) uv: vec2<f32>,
};

@group(0) @binding(0)
var<uniform> view_proj: mat4x4<f32>;
@group(1) @binding(0)
var<uniform> model: mat4x4<f32>;
@group(1) @binding(1)
var tex: texture_2d<f32>;
@group(1) @binding(2)
var sam: sampler;

struct Varyings {
    @builtin(position) clip_pos: vec4<f32>,
    @location(0) color: vec4<f32>,
    @location(1) uv: vec2<f32>,
};

@vertex
fn vert(in: VertexData) -> Varyings {
    var out: Varyings;
    out.clip_pos = view_proj * model * vec4<f32>(in.position, 0.0, 1.0);
    out.uv = in.uv;
    out.color = in.color;
    return out;
}

@fragment
fn frag(in: Varyings) -> @location(0) vec4<f32> {
    return in.color * textureSample(tex, sam, in.uv);
}